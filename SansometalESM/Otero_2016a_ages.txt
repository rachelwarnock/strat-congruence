	FAD	LAD
Yunguisaurus_liae_ORIG_Yunguisaurus_liae	242	237
Pistosaurus_ORIG_Pistosaurus_skull	247.2	237
Augustasaurus_hagdorni_ORIG_Augustasaurus_hagdorni	247.2	242
Bobosaurus_forojuliensis_ORIG_Bobosaurus_forojuliensis	235	232
Macroplata_tenuiceps_ORIG_Macroplata_tenuiceps	201.3	196.5
Anningasaura_lymense_ORIG_Anningasaura_lymense	201.3	183
Stratesaurus_taylori_ORIG_Stratesaurus_taylori	201.3	196.5
Avalonnectes_arturi_ORIG_Avalonnectes_arturi	201.3	196.5
Eurycleidus_arcuatus_ORIG_Eurycleidus_arcuatus	201.3	183
Meyerasaurus_victor_ORIG_Meyerasaurus_victor	183	182
Maresaurus_coccai_ORIG_Maresaurus_coccai	171.6	168.4
Borealonectes_russelli_ORIG_Borealonectes_russelli	166.1	163.5
Rhomaleosaurus_megacephalus_ORIG_Rhomaleosaurus_megacephalus	208.5	196.5
Archaeonectrus_rostratus_ORIG_Archaeonectrus_rostratus	208.5	190.8
Rhomaleosaurus_cramptoni_ORIG_Rhomaleosaurus_cramptoni	183	174.1
Rhomaleosaurus_zetlandicus_ORIG_Rhomaleosaurus_zetlandicus	190.8	174.1
Thalassiodracon_hawkinsii_ORIG_Thalassiodracon_hawkinsii	208.5	183
Hauffiosaurus_longirostris_ORIG_Hauffiosaurus_longirostris	183	175.6
Hauffiosaurus_tomistomimus_ORIG_Hauffiosaurus_tomistomimus	182	175.6
Hauffiosaurus_zanoni_ORIG_Hauffiosaurus_zanoni	183	182
Marmornectes_candrewi_ORIG_Marmornectes_candrewi	164.7	161.2
Peloneustes_philarchus_ORIG_Peloneustes_philarchus	166.1	161.2
Simolestes_vorax_ORIG_Simolestes_vorax	164.7	161.2
Pliosaurus_funkei_ORIG_Pliosaurus_funkei	152.1	145
Pliosaurus_ORIG_Pliosaurus_BRSMG_Cc332	166.1	66
Pliosaurus_brachydeirus_ORIG_Pliosaurus_brachydeirus	157.3	145
Gallardosaurus_iturraldei_ORIG_Gallardosaurus_iturraldei	161.2	155.7
Pliosaurus_rossicus_ORIG_Liopleurodon_rossicus	155.7	145
Pliosaurus_andrewsi_ORIG_Pliosaurus_andrewsi	164.7	157.3
Liopleurodon_ferox_ORIG_Liopleurodon_ferox	166.1	157.3
Kronosaurus_ORIG_Kronosaurus_MCZ_1285	125	99.6
Megacephalosaurus_eulerti_ORIG_Brachauchenius_eulerti	93.5	89.3
Brachauchenius_lucasi_ORIG_Brachauchenius_lucasi	100.5	89.3
Brachauchenius_ORIG_Brachauchenius_MNA_V9433	130	89.3
Attenborosaurus_conybeari_ORIG_Attenborosaurus_conybeari	199.3	189.6
Plesiosaurus_dolichodeirus_ORIG_Plesiosaurus_dolichodeirus	201.3	183
Eoplesiosaurus_antiquior_ORIG_Eopleiosaurus_antiquior	201.3	196.5
Eretmosaurus_rugosus_ORIG_Eretmosaurus_rugosus	196.5	189.6
Westphaliasaurus_simonsensii_ORIG_Westphaliasaurus_simonsensii	189.6	183
Seeleyosaurus_guilelmiimperatoris_ORIG_Seelyosaurus_guilelmiimperatoris	183	182
Microcleidus_tournemirensis_ORIG_Microcleidus_tournemirensis	180.1	175.6
Microcleidus_brachypterygius_ORIG_Microcleidus_brachypterygius	183	182
Microcleidus_homalospondylus_ORIG_Microcleidus_homalospondylus	183	175.6
Plesiopterys_wildi_ORIG_Plesiopterys_wildi	183	182
Cryptoclidus_eurymerus_ORIG_Cryptoclidus_eurymerus	166.1	157.3
Tricleidus_seeleyi_ORIG_Tricleidus_seeleyi	166.1	161.2
Muraenosaurus_leedsii_ORIG_Muraenosaurus_leedsii	166.1	157.3
Kimmerosaurus_langhami_ORIG_Kimmerosaurus_langhami	157.3	145
Pantosaurus_striatus_ORIG_Pantosaurus_striatus	163.5	155.7
Picrocleidus_beloclis_ORIG_Picrocleidus_beloclis	164.7	161.2
Tatenectes_laramiensis_ORIG_Tatenectes_laramiensis	163.5	155.7
Colymbosaurus_trochanterius_ORIG_Colymbosaurus_trochanterius	167.7	145
Djupedalia_engeri_ORIG_Djupedalia_engeri	152.1	145
Spitrasaurus_ORIG_Spitrasaurus_spp	152.1	145
Abyssosaurus_nataliae_ORIG_Abyssosaurus_nataliae	136.4	130
Umoonasaurus_demoscyllus_ORIG_Umoonasaurus_demoscyllus	125	100.5
Nichollssaura_borealis_ORIG_Nichollssaura_borealis	113	100.5
Leptocleidus_capensis_ORIG_Leptocleidus_capensis	140.2	136.4
Leptocleidus_superstes_ORIG_Leptocleidus_superstes	129.4	125
Hastanectes_valdensis_ORIG_Cimoliasaurus_valdensis	145	136.4
Brancasaurus_brancai_ORIG_Brancasaurus_brancai	145	140.2
Plesiosauria_ORIG_Speeton_Clay_plesiosaurian	228	61.6
Wapuskanectes_betsynichollsae_ORIG_Wapuskanectes_betsynichollsae	112.03	109
Futabasaurus_suzukii_ORIG_Futabasaurus_suzukii	85.8	83.5
Callawayasaurus_colombiensis_ORIG_Callawayasaurus_colombiensis	125.45	122.46
Eromangasaurus_carinognathus_ORIG_Eromangasaurus_carinognathus	105.3	93.9
Libonectes_morgani_ORIG_Libonectes_morgani	93.5	89.3
Hydrotherosaurus_alexandrae_ORIG_Hydrotherosaurus_alexandrae	72.1	66
Edgarosaurus_muddi_ORIG_Edgarosaurus_muddi	105.3	99.6
Palmulasaurus_quadratus_ORIG_Palmulasaurus_quadratus	93.5	89.3
Pahasapasaurus_haasi_ORIG_Pahasapasaurus_haasi	99.6	93.5
Eopolycotylus_rankini_ORIG_Eopolycotylus_rankini	99.6	89.3
Polycotylus_latipinnis_ORIG_Polycotylus_latipinnis	89.3	70.6
Plesiopleurodon_wellesi_ORIG_Plesiopleurodon_wellesi	99.6	93.5
Manemergus_anguirostris_ORIG_Manemergus_anguirostris	93.5	89.3
Dolichorhynchops_tropicensis_ORIG_Dolichorhynchops_tropicensis	93.5	89.3
Trinacromerum_bentonianum_ORIG_Trinacromerum_bentonianum	99.6	89.3
Dolichorhynchops_bonneri_ORIG_Dolichorhynchops_bonneri	83.6	70.6
Dolichorhynchops_osborni_ORIG_Dolichorhynchops_osborni	93.9	70.6
Georgiasaurus_penzensis_ORIG_Georgiasaurus_penzensis	86.3	83.6
Dolichorhynchops_herschelensis_ORIG_Dolichorhynchops_herschelensis	83.5	66
Aristonectes_quiriquiensis_ORIG_Aristonectes_quiriquinensis	70.6	66
Aristonectes_parvidens_ORIG_Aristonectes_parvidens	83.5	66
Kaiwhekea_katiki_ORIG_Kaiwhekea_katiki	83.5	66
Alexandronectes_zealandiensis_ORIG_Alexandronectes_zealandiensis	83.5	66
Tuarangisaurus_keyesi_ORIG_Tuarangisaurus_keyesi	85.8	66
Thalassomedon_hanningtoni_ORIG_Thalassomedon_haningtoni	99.6	93.5
Styxosaurus_snowii_ORIG_Styxosaurus_snowii	83.5	70.6
Albertonectes_vanderveldei_ORIG_Albertonectes_vanderveldei	83.5	70.6
Elasmosaurus_platyurus_ORIG_Elasmosaurus_platyurus	84.9	70.6
Terminonatator_ponteixensis_ORIG_Terminonatator_pointeixensis	83.5	70.6
