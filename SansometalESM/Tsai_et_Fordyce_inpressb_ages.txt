	FAD	LAD
Zygorhiza_kochii_ORIG_Zygorhiza_kochii	38	33.9
Archaeodelphis_patrius_ORIG_Archaeodelphis_patrius	28.1	23.03
Waipatia_maerewhenua_ORIG_Waipatia_maerewhenua	27.3	25.2
Physeter_macrocephalus_ORIG_Physeter_catodon	5.333	0
Aetiocetus_cotylalveus_ORIG_Aetiocetus_cotylalveus	33.9	23.03
Aetiocetus_polydentatus_ORIG_Aetiocetus_polydentatus	28.1	23.03
Aetiocetus_weltoni_ORIG_Aetiocetus_weltoni	28.1	23.03
Aglaocetus_moreni_ORIG_Aglaocetus_moreni	20.44	15.97
Aglaocetus_patulus_ORIG_Aglaocetus_patulus	15.97	13.82
Archaebalaenoptera_castriarquati_ORIG_Archaebalaenoptera_castriarquati	3.6	2.588
Balaena_montalionis_ORIG_Balaena_montalionis	3.6	2.588
Balaena_mysticetus_ORIG_Balaena_mysticetus	2.588	0
Balaena_ricei_ORIG_Balaena_ricei	5.333	3.6
Balaenella_brachyrhynus_ORIG_Balaenella_brachyrhynus	5.333	3.6
Balaenoptera_acutorostrata_ORIG_Balaenoptera_acutorostrata	5.333	0
Balaenoptera_bertae_ORIG_Balaenoptera_bertae	7.246	2.588
Balaenoptera_bonaerensis_ORIG_Balaenoptera_bonaerensis	0.0117	0
Balaenoptera_borealis_ORIG_Balaenoptera_borealis	15.97	0
Balaenopteridae_ORIG_Balaenoptera_portisi	28.4	0
Balaenoptera_musculus_ORIG_Balaenoptera_musculus	1.806	0
Balaenoptera_omurai_ORIG_Balaenoptera_omurai	0.0117	0
Balaenoptera_physalus_ORIG_Balaenoptera_physalus	2.588	0
Balaenoptera_ORIG_Balaenoptera_ryani	15.97	0
Balaenoptera_siberi_ORIG_Balaenoptera_siberi	11.62	5.333
Balaenula_astensis_ORIG_Balaenula_astensis	5.333	2.588
Balaenula_ORIG_Balaenula_sp	20.44	0.0117
Brandtocetus_chongulek_ORIG_Brandtocetus_chongulek	12.7	11.608
Caperea_marginata_ORIG_Caperea_marginata	0.0117	0
Cephalotropis_coronatus_ORIG_Cephalotropis_coronatus	11.62	7.246
Tranatocetidae_ORIG_Cetotherium_megalophysum	20.44	2.588
Cetotherium_rathkii_ORIG_Cetotherium_rathkii	13.65	7.246
Cetotherium_riabinini_ORIG_Cetotherium_riabinini	11.62	7.246
Fucaia_goedertorum_ORIG_Chonecetus_goedertorum	28.1	23.03
Chonecetus_sookensis_ORIG_Chonecetus_sookensis	28.1	23.03
Diorocetus_chichibuensis_ORIG_Diorocetus_chichibuensis	20.44	13.82
Diorocetus_hiatus_ORIG_Diorocetus_hiatus	15.97	13.82
Diorocetus_shobarensis_ORIG_Diorocetus_shobarensis	20.44	13.82
Diunatans_luctoretemergo_ORIG_Diunatans_luctoretemergo	23.03	2.588
Eomysticetus_whitmorei_ORIG_Eomysticetus_whitmorei	28.1	23.03
Eschrichtioides_gastaldii_ORIG_Eschrichtioides_gastaldii	5.333	2.588
Eschrichtius_robustus_ORIG_Eschrichtius_robustus	2.588	0
Balaenidae_ORIG_Eubalaena_belgica	20.44	0
Eubalaena_shinshuensis_ORIG_Eubalaena_shinshuensis	7.246	5.333
Eubalaena_ORIG_Eubalaena_spp	7.246	0
Gricetoides_aurorae_ORIG_Gricetoides_aurorae	5.333	3.6
Herpetocetus_bramblei_ORIG_Herpetocetus_bramblei	7.246	3.6
Herpetocetus_morrowi_ORIG_Herpetocetus_morrowi	3.6	2.588
Herpetocetus_ORIG_Herpetocetus_sp	23.03	0.0117
Herpetocetus_transatlanticus_ORIG_Herpetocetus_transatlanticus	5.333	3.6
Isanacetus_laticephalus_ORIG_Isanacetus_laticephalus	20.44	15.97
Janjucetus_hunderi_ORIG_Janjucetus_hunderi	28.1	23.03
Joumocetus_shimizui_ORIG_Joumocetus_shimizui	11.62	7.246
Kurdalagonus_mchedlidzei_ORIG_Kurdalagonus_mchedlidzei	13.82	7.246
Llanocetus_denticrenatus_ORIG_Llanocetus_denticrenatus	38	33.9
Mammalodon_colliveri_ORIG_Mammalodon_colliveri	28.1	23.03
Mauicetus_parki_ORIG_Mauicetus_parki	28.1	21.7
Megaptera_ORIG_Megaptera_miocaena	11.62	0
Megaptera_novaeangliae_ORIG_Megaptera_novaeangliae	2.588	0
Metopocetus_durinasus_ORIG_Metopocetus_durinasus	15.97	11.62
Micromysticetus_rothauseni_ORIG_Micromysticetus_rothauseni	33.9	28.1
Miocaperea_pulchra_ORIG_Miocaperea_pulchra	11.62	7.246
Morawanocetus_yabukii_ORIG_Morawanocetus_yabukii	28.1	23.03
Morenocetus_parvus_ORIG_Morenocetus_parvus	20.44	15.97
Nannocetus_eremus_ORIG_Nannocetus_eremus	11.62	5.333
Parabalaenoptera_baulinensis_ORIG_Parabalaenoptera_baulinensis	7.246	5.333
Parietobalaena_campiniana_ORIG_Parietobalaena_campiniana	23.03	2.588
Parietobalaena_palmeri_ORIG_Parietobalaena_palmeri	20.44	13.82
Parietobalaena_ORIG_Parietobalaena_sp	23.03	2.588
Parietobalaena_yamaokai_ORIG_Parietobalaena_yamaokai	20.44	13.82
Pelocetus_calvertensis_ORIG_Pelocetus_calvertensis	15.97	13.82
Peripolocetus_vexillifer_ORIG_Peripolocetus_vexillifer	15.97	13.82
Pinocetus_polonicus_ORIG_Pinocetus_polonicus	15.97	13.82
Piscobalaena_nana_ORIG_Piscobalaena_nana	13.82	5.333
Plesiobalaenoptera_quarantellii_ORIG_Plesiobalaenoptera_quarantellii	11.62	7.246
Thinocetus_arthritus_ORIG_Thinocetus_arthritus	13.82	11.62
Tiphyocetus_temblorensis_ORIG_Tiphyocetus_temblorensis	15.97	13.82
Titanocetus_sammarinensis_ORIG_Titanocetus_sammarinensis	20.44	13.82
Uranocetus_gramensis_ORIG_Uranocetus_gramensis	11.62	7.246
Fucaia_buelli_ORIG_Fucaia_buelli	33.9	28.4
Yamatocetus_canaliculatus_ORIG_Yamatocetus_canaliculatus	28.1	23.03
Horopeta_umarere_ORIG_Horopeta_umarere	27.3	25.2
Whakakai_waipata_ORIG_Whakakai_waipata	27.3	25.2
