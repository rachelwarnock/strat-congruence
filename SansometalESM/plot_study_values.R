library(ggplot2)
library(gridExtra)

# creat subdir to store output
system("mkdir out")

# open study values
csv = read.csv("_RESULTSALLSTRAPcomb2.csv")

total = length(unique(csv$Dataset)) # 167

# keep track of the number of times the MAP trees is more
# stratigraphically congruent
sci.ML.better = c()
rci.ML.better = c()
ger.ML.better = c()
msm.ML.better = c()

sci.MAP.better = c()
rci.MAP.better = c()
ger.MAP.better = c()
msm.MAP.better = c()

n = 1000

for(i in 1:total){
  
  dataset = unique(csv$Dataset)[i]
  
  # subset the data by dataset
  sub = csv[which(csv$Dataset == dataset),]
  
  # subset Bayesian
  bayesian = sub[which(sub$Method == "Bayesian"),]
  
  # subset equal weights Parsimony
  parsimony = sub[which(sub$Method == "Parsimony_EQ"),]
  
  # read mcmc output -- do you need this?
  mcmc = read.table(paste0("mrbayes/", dataset, "_paleobiodb.nex.run1.p"), header = TRUE, skip = 1)
  
  # read mcmc trees
  trees = ape::read.nexus(paste0("mrbayes/", dataset, "_paleobiodb.nex.run1.t"))
  
  # find the maximum likelihood...
  ML = max(mcmc$LnL)
  # ...and the associated tree
  ML.tree = trees[[which(mcmc$LnL == ML)]]
  
  # calculate the posterior probability for each tree
  mcmc$LnPosterior = mcmc$LnL + mcmc$LnPr
  
  # find the maximum posterior...
  MAP = max(mcmc$LnPosterior)
  # ...and the associated tree
  MAP.tree = trees[[which(mcmc$LnPosterior == MAP)]]
  
  # another way of getting there
  # gen = mcmc$Gen[which(mcmc$LnL == ML)]
  # which(names(trees) == paste0("gen.", gen))
  
  # identify the outgroup
  outgroup = system(paste0("cat ", paste0(dataset, "_paleobiodb.nex"), " | grep outgroup"), intern = TRUE)
  outgroup = gsub("outgroup ", "", outgroup)
  outgroup = gsub(";", "", outgroup)
  
  if(i == 121) outgroup = outgroup[2]
  
  # root the trees
  ML.tree = ape::root(ML.tree, outgroup = outgroup, resolve.root = TRUE)
  MAP.tree = ape::root(MAP.tree, outgroup = outgroup, resolve.root = TRUE)
  
  # prune tips without any age data
  for(j in ML.tree$tip.label){
    if(grepl("no_age_data", j) | grepl("missing_ORIG", j) | grepl("duplicate_ORIG", j)){
      ML.tree = ape::drop.tip(ML.tree, j)
      MAP.tree = ape::drop.tip(MAP.tree, j)
    }
  }
  
  # read age data
  paleo = read.table(paste0(dataset, "_ages.txt"))
  
  # calculate stratigraphic congruence for the ML.tree
  out = strap::StratPhyloCongruence(ML.tree, paleo, samp.perm = n, rand.perm = n)
  
  sci.ML = out$input.tree.results[1]
  rci.ML = out$input.tree.results[2]
  ger.ML = out$input.tree.results[3]
  msm.ML = out$input.tree.results[4]
  
  # calculate stratigraphic congruence for the ML.tree
  out = strap::StratPhyloCongruence(MAP.tree, paleo, samp.perm = n, rand.perm = n)
  
  sci.MAP = out$input.tree.results[1]
  rci.MAP = out$input.tree.results[2]
  ger.MAP = out$input.tree.results[3]
  msm.MAP = out$input.tree.results[4]
  
  sci.pars = max(parsimony$SCI)
  rci.pars = max(parsimony$RCI)
  ger.pars = max(parsimony$GER)
  msm.pars = max(parsimony$MSM.)
  
  # from Bell & Lloyd:
  # SCI ranges from zero to one (= maximally consistent)
  sci.ML.better[i] = ifelse(sci.ML >= sci.pars, 1, 0)
  sci.MAP.better[i] = ifelse(sci.MAP >= sci.pars, 1, 0)
  
  # RCI is intended to be a percentage but can be negative or > 100
  rci.ML.better[i] = ifelse(rci.ML >= rci.pars, 1, 0)
  rci.MAP.better[i] = ifelse(rci.MAP >= rci.pars, 1, 0)
  
  # GER ranges from zero to one (= maximally consistent)
  ger.ML.better[i] = ifelse(ger.ML >= ger.pars, 1, 0)
  ger.MAP.better[i] = ifelse(ger.MAP >= ger.pars, 1, 0)
  
  # MSM ranges from zero to one (= maximally consistent)
  msm.ML.better[i] = ifelse(msm.ML >= msm.pars, 1, 0)
  msm.MAP.better[i] = ifelse(msm.MAP >= msm.pars, 1, 0)
  
  plot = TRUE
  if(plot){
    # SCI
    p1 = ggplot(data = bayesian) + geom_histogram(aes(x = SCI), bins = 10,
                                             fill = "dodgerblue3") +
    geom_vline(xintercept = parsimony$SCI, colour = "tomato", lty = 2, lwd = 1) +
      geom_vline(xintercept = sci.ML, colour = "black", lty = 2, lwd = 1) +
      geom_vline(xintercept = sci.MAP, colour = "black", lty = 1, lwd = 1)
    
    # RCI
    p2 = ggplot(data = bayesian) + geom_histogram(aes(x = RCI), bins = 15,
                                             fill = "dodgerblue3") +
      geom_vline(xintercept = parsimony$RCI, colour = "tomato", lty = 2, lwd = 1) +
      geom_vline(xintercept = rci.ML, colour = "black", lty = 2, lwd = 1) +
      geom_vline(xintercept = rci.MAP, colour = "black", lty = 1, lwd = 1)
    
    # GER
    p3 = ggplot(data = bayesian) + geom_histogram(aes(x = GER), bins = 15,
                                             fill = "dodgerblue3") +
      geom_vline(xintercept = parsimony$GER, colour = "tomato", lty = 2, lwd = 1) +
      geom_vline(xintercept = ger.ML, colour = "black", lty = 2, lwd = 1) +
      geom_vline(xintercept = ger.MAP, colour = "black", lty = 1, lwd = 1)
    
    # MSM
    p4 = ggplot(data = bayesian) + geom_histogram(aes(x = MSM.), bins = 15,
                                             fill = "dodgerblue3") +
      geom_vline(xintercept = parsimony$MSM., colour = "tomato", lty = 2, lwd = 1) +
      geom_vline(xintercept = msm.ML, colour = "black", lty = 2, lwd = 1) +
      geom_vline(xintercept = msm.MAP, colour = "black", lty = 1, lwd = 1)
   
    pdf(paste0("out/", dataset,"_hist.pdf"), width = 8, height = 10)
    grid.arrange(p1,
                 p2,
                 p3,
                 p4,
                 nrow = 2, ncol = 2, top = as.character(dataset))
    dev.off()
  }
  
  #eof
}

sum(sci.ML.better)/length(sci.ML.better)
sum(rci.ML.better)/length(rci.ML.better)
sum(ger.ML.better)/length(ger.ML.better)
sum(msm.ML.better)/length(msm.ML.better)

sum(sci.MAP.better)/length(sci.MAP.better)
sum(rci.MAP.better)/length(rci.MAP.better)
sum(ger.MAP.better)/length(ger.MAP.better)
sum(msm.MAP.better)/length(msm.MAP.better)


# try with the consensus tree?
#tree = ape::read.nexus("mrbayes/Abella_etal_2012a_paleobiodb.nex.con.tre")

