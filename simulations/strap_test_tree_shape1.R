library(ggplot2)

### functions

# calculate node ages (internal FossilSim function)
n.ages = function(tree){
  
  depth = ape::node.depth.edgelength(tree)
  node.ages = max(depth) - depth
  names(node.ages) <- 1:(tree$Nnode+length(tree$tip))
  
  # adding possible offset if tree fully extinct
  if(!is.null(tree$root.time)) node.ages = node.ages + tree$root.time - max(node.ages)
  
  return(node.ages)
}

strat.congruence = function(trees, fossils, random.tree = FALSE){
  
  sci = c()
  rci = c()
  ger = c()
  msm = c()
  sci.p = c()
  rci.p = c()
  ger.p = c()
  msm.p = c()
  gers = c()
  gert = c()
  mig = c()
  p.wills = c()
  
  for(i in 1:reps){
    
    if(length(fossils[[i]]$sp) == 0) next
    
    # add fossils to the tree as zero length edges = SA tree
    t2 = FossilSim::SAtree.from.fossils(trees[[i]],fossils[[i]])
    # remove all unsampled lineages
    t3 = FossilSim::sampled.tree.from.combined(t2)
    # remove all intermediate fossils (i.e. all fossils except FAs and LAs)
    t4 = FossilSim::prune.fossils(t3)
    
    #if(length(fossils[[i]]$sp) == 0) t4$tip.label = gsub("_last","_first",t4$tip.label)
    
    # collect FA & LA data
    names = c()
    FA = c()
    LA = c()
    
    tmp = t4
    
    # get rid of "first" and "last" labels
    tmp$tip.label = gsub("_first","",gsub("_last","",t4$tip.label))
    
    for(j in unique(tmp$tip.label)){
      times = n.ages(tmp)[which(tmp$tip.label == j)]
      names = c(names, j)
      FA = c(FA, max(times))
      LA = c(LA, min(times))
    }
    
    ages = data.frame(row.names = names, FAD = FA, LAD = LA)
    
    # parse input tree. drop last appearances (singletons are represented by first only)
    t5 = ape::drop.tip(t4, t4$tip.label[which(grepl("last", t4$tip.label))])
    t5$tip.label = gsub("_first","",t5$tip.label)
    # change class from SA tree to phylo (strap takes as input an object of class phylo)
    class(t5) = "phylo"
    
    # generate random tree topology
    if(random.tree){
      tips = t5$tip.label
      obs = t5$edge.length
      t5 = ape::rtree(length(t5$tip.label))
      # Draw edge lengths from a uniform distribution between the limits of observed edge lengths:
      t5$edge.length = runif(length(t5$edge.length), min = min(obs), max = max(obs))
      t5$tip.label = tips
    }
    
    # measure stratigraphic congruence
    out = strap::StratPhyloCongruence(t5, ages, rand.perm = rand.perm, samp.perm = samp.perm)
    
    # collect output
    sci = c(sci, out$input.tree.results[1])
    rci = c(rci, out$input.tree.results[2])
    ger = c(ger, out$input.tree.results[3])
    msm = c(msm, out$input.tree.results[4])
    
    sci.p = c(sci.p, out$input.tree.results[5])
    rci.p = c(rci.p, out$input.tree.results[6])
    ger.p = c(ger.p, out$input.tree.results[7])
    msm.p = c(msm.p, out$input.tree.results[8])
    
    gers = c(gers, out$input.tree.results[9])
    gert = c(gert, out$input.tree.results[10])
    mig = c(mig, out$input.tree.results[11])
    p.wills = c(p.wills, out$input.tree.results[12])
    
  }
  
  df = data.frame(value = c(sci, rci, ger, msm, gers, gert, mig),
                  p.vales = c(sci.p, rci.p, ger.p, msm.p, rep(p.wills,3)),
                  method = c(rep("SCI", length(sci)), 
                             rep("RCI", length(rci)),
                             rep("GER", length(ger)), 
                             rep("MSM", length(msm)), 
                             rep("GERs", length(gers)), 
                             rep("GERt", length(gert)),
                             rep("MIG", length(msm))), 
                  rate = rate)
   
  return(df)
}

###

file = "tree_shape1.log"
cat(NULL, file = file, append = FALSE)

rand.perm = 100
samp.perm = 100

set.seed(1234)

# Simulate trees
reps = 100
lambda = 0.1
mu = 0.05
tips = 25

# fully balanced tree

# t = TreeSim::sim.bd.taxa(tips, reps, lambda, mu)

# mx = unlist(lapply(t, function(x){max(TreeSim::getx(x, sersampling = 1)[,1])}))

tree = ape::read.tree(text = "((((t1, t2), (t3, t4)), ((t5, t6), (t7, t8))), (((t9, t10), (t11, t12)), ((t13, t14), (t15, t16))));")

tree$edge.length = rep(11, length(tree$edge[,2]))

t = rep(list(tree), reps)

# simulate fossils assumming bifurcating speciation

rate = 0.1

f = lapply(t, function(x){FossilSim::sim.fossils.poisson(rate, x)})

rate = "balanced" # fig label

df1 = strat.congruence(t, f)
cat("done df1\n", file = file, append = TRUE)

df1r = strat.congruence(t, f, random.tree = TRUE)
cat("done df1r\n", file = file, append = TRUE)

# fully balanced tree

tree = ape::read.tree(text = "(t1:44, (t2: 41.06667, (t3:38.13333, (t4:35.2, (t5:32.26667, (t6:29.33333, (t7:26.4, (t8:23.46667, (t9:20.53333, (t10:17.6, (t11:14.66667, (t12:11.73333, (t13:8.8, (t14:5.866667, (t15:2.933333, t16:2.933333):2.933333):2.933333):2.933333):2.933333):2.933333):2.933333):2.933333):2.933333):2.933333):2.933333):2.933333):2.933333):2.933333):2.933333);")

t = rep(list(tree), reps)

rate = 0.1

# simulate fossils assumming budding speciation

s = lapply(t, function(x){FossilSim::sim.taxonomy(x)})

f = lapply(s, function(x){FossilSim::sim.fossils.poisson(rate, taxonomy = x)})

rate = "unbalanced" # fig label

df2 = strat.congruence(t, f)
cat("done df2\n", file = file, append = TRUE)

df2r = strat.congruence(t, f, random.tree = TRUE)
cat("done df2r\n", file = file, append = TRUE)

#rate = 0.01

#f = lapply(t, function(x){FossilSim::sim.fossils.poisson(rate, x)})

#df3 = strat.congruence(t, f)
#cat("done df3\n", file = file, append = TRUE)

#df3r = strat.congruence(t, f, random.tree = TRUE)
#cat("done df3r\n", file = file, append = TRUE)

#df = data.frame(rbind(data.frame(cbind(tree = "true"), data.frame(rbind(df1,df2,df3))),
#                      data.frame(cbind(tree = "random"), data.frame(rbind(df1r,df2r,df3r)))))

df = data.frame(rbind(data.frame(cbind(tree = "true"), data.frame(rbind(df1,df2))),
                                            data.frame(cbind(tree = "random"), data.frame(rbind(df1r,df2r)))))

### print output

write.table(df, row.names = FALSE, quote = TRUE, file = "tree_shape1.txt")

# df = read.table("fossil_recovery.txt", header = TRUE) # -> will produce plots with "true" & "random" the other way around

### plot output (total)

# pdf("fossil_recovery_total.pdf", width = 8, height = 10)
# ggplot(subset(df, p.vales < 0.05), aes(x = as.factor(rate), y = value)) + geom_boxplot(aes(fill = tree)) +
#   facet_grid(method~., scales = "free") + ggtitle("The impact of fossil sampling") +
#   xlab("Fossil recovery rate") + ylab("Stratigraphic congruence")
# dev.off()

### plot output (subset)

pdf("tree_shape1.pdf", width = 8, height = 8)
ggplot(subset(df, p.vales < 0.05 & method != "GERt" & method != "GERs" & method != "MIG"), 
       aes(x = as.factor(rate), y = value)) + geom_boxplot(aes(fill = tree)) + 
  facet_grid(method~., scales = "free") + ggtitle("The impact of tree shape") +
  xlab("Tree shape") + ylab("Stratigraphic congruence")
dev.off()

###

pdf("tree_shape1_true.pdf", width = 8, height = 8)
ggplot(subset(df, p.vales < 0.05 & method != "GERt" & method != "GERs" & method != "MIG" & tree == "true"), 
       aes(x = as.factor(rate), y = value)) + geom_boxplot(aes(fill = tree)) + 
  facet_grid(method~., scales = "free") + ggtitle("The impact of tree shape") +
  xlab("Tree shape") + ylab("Stratigraphic congruence")
dev.off()
